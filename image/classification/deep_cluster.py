import tensorflow as tf
import sklearn.cluster
from sklearn.metrics.cluster import normalized_mutual_info_score
import numpy as np
from abc import ABCMeta, abstractmethod


class SimpleDeepClusterBase(metaclass=ABCMeta):
    FULLY_CONNECTION_CLASSIFER = "fully_connection_classifer"

    def __init__(self, cluster_num):
        self._kmeans = None
        self._cluster_num = cluster_num
        self._session = None
        self._train_step = None
        self._x = None
        self._mapped_x = None
        self._cross_entropy = None
        self._t = None
        self._predict_result = None
        self._is_train = None

    def init_model(self, input_shape, mapped_num, learning_rate):
        """
        setup DeepCluster model.
        :param input_shape:
        :param mapped_num: number of neuron mapped.
        :param learning_rate:
        """
        tf.reset_default_graph()
        self._is_train = tf.placeholder(dtype=tf.bool)
        self._x = tf.placeholder("float32", (None,) + input_shape)
        self._mapped_x = self.convolution_net_map_layers(self._x, mapped_num, self._is_train)
        with tf.variable_scope(self.FULLY_CONNECTION_CLASSIFER):
            self._predict_result = self.classifier_layers(self._mapped_x, self._cluster_num, self._is_train)
        self._t = tf.placeholder("float32", (None, self._cluster_num))
        self._cross_entropy = - tf.reduce_mean(self._t * tf.log(self._predict_result))
        self._train_step = tf.train.AdamOptimizer(learning_rate).minimize(self._cross_entropy)
        gpu_config = tf.ConfigProto(
            gpu_options=tf.GPUOptions(per_process_gpu_memory_fraction=0.8,
                                      allow_growth=True),
            device_count={'GPU': 1})
        self._session = tf.Session(config=gpu_config)
        init = tf.global_variables_initializer()
        self._session.run(init)

    def train(self, x_list, epochs, y_list=None, test_x_list=None, test_y_list=None, rebuild_kmeans_epochs=1):
        """
        build DeepCluster classifer.
        :param x_list: train data
        :param epochs: number of epoch
        :param y_list: teacher
        :param test_x_list: test data
        :param test_y_list: test teacher data
        :param rebuild_kmeans_epochs: number of what to rebuild clustering classifer per epoch.
        """
        for epoch in range(epochs):
            if epoch % rebuild_kmeans_epochs == 0:
                # rebuild clustering classifer.
                self._build_clustering_model(x_list)
                print("======= rebuild clustering =======")
            t_list = []
            for x in x_list:
                mapped_x = self._session.run(self._mapped_x, feed_dict={self._is_train: False, self._x: [x]})
                # build pseudo-created label.
                t = np.zeros(self._cluster_num)
                t[self._kmeans.predict(mapped_x)] = 1
                t_list.append(t)
                self._session.run(self._train_step, feed_dict={self._is_train: True, self._x: [x], self._t: [t]})
            t_list = np.argmax(t_list, axis=1)
            self._output_progress(epoch + 1, x_list, t_list, y_list, test_x_list, test_y_list)

    def accuracy(self, x_list, y_list):
        """
        get accuracy of classifier.
        :param x_list: test data
        :param y_list: test teacher data
        :return: normalized mutual information of classifier.
        """
        predict_result_list = self._session.run(self._predict_result, feed_dict={
            self._is_train: False, self._x: x_list
        })
        return normalized_mutual_info_score(y_list, np.argmax(predict_result_list, 1))

    def clustering_classifier_accuracy(self, x_list, y_list):
        """
        get accuracy of clustering classifier.
        :param x_list: test data
        :param y_list: test teacher data
        :return: normalized mutual information of clustering classifier.
        """
        mapped_x_list = self._session.run(self._mapped_x, feed_dict={self._is_train: False, self._x: x_list})
        return normalized_mutual_info_score(y_list, self._kmeans.predict(mapped_x_list))

    def save(self, filepath):
        """
        save model.
        :param filepath:
        """
        saver = tf.train.Saver()
        saver.save(self._session, filepath)

    def load(self, filepath):
        """
        load model.
        :param filepath:
        """
        saver = tf.train.Saver()
        saver.restore(self._session, filepath)

    @abstractmethod
    def convolution_net_map_layers(self, x, mapped_num, is_train, kernel_size=(3, 3)):
        """
        setup CNN mapping layers.
        :param x:
        :param mapped_num: number of neuron mapped.
        :param is_train: tf.bool, if train or other.
        :param kernel_size:
        :return: a network that outputs "mapped_num" number neuron.
        """
        pass

    @abstractmethod
    def classifier_layers(self, x, cluster_num, is_train):
        """
        setup classifier layers.
        :param x:
        :param cluster_num:
        :param is_train: tf.bool, if train or other.
        :return: a network that outputs "cluster_num" number neuron.
        """
        pass

    def _build_clustering_model(self, x_list):
        """
        build the clustering classifier.
        :param x_list: dataset
        """
        # map using CNN.
        mapped_x_list = self._session.run(self._mapped_x, feed_dict={self._is_train: False, self._x: x_list})
        # build clustering classifier using CNN mapped data.
        self._kmeans = sklearn.cluster.KMeans(self._cluster_num, verbose=0)
        self._kmeans.fit(np.reshape(mapped_x_list, [len(mapped_x_list), -1]))
        # 全結合分類レイヤの重みをクリアする
        # clear classifier layers.
        init = tf.variables_initializer(tf.global_variables(self.FULLY_CONNECTION_CLASSIFER))
        self._session.run(init)

    def _output_progress(self, epoch_num, x_list, t_list, y_list=None, test_x_list=None, test_y_list=None):
        print("=========  epoch {}  =========".format(epoch_num))
        if y_list is not None:
            # compare if teacher data exists.
            print("train_acc : {}".format(self.accuracy(x_list, y_list)))
        # compare to pseudo-created label.
        print("preudo_train_acc : {}".format(self.accuracy(x_list, t_list)))
        if test_x_list is not None and test_y_list is not None:
            # accuracy using test data.
            print("test_acc : {}".format(self.accuracy(test_x_list, test_y_list)))
            print("kmeans acc : {}".format(self.clustering_classifier_accuracy(test_x_list, test_y_list)))
            print("==============================\n")


class SimpleDeepCluster(SimpleDeepClusterBase):
    def convolution_net_map_layers(self, x, mapped_num, is_train, kernel_size=(3, 3)):
        """
        setup CNN mapping layers.
        :param x:
        :param mapped_num: number of neuron mapped.
        :param is_train: tf.bool, if train or other.
        :param kernel_size:
        :return: a network that outputs "mapped_num" number neuron.
        """
        conv_1 = tf.layers.conv2d(inputs=x, filters=32, kernel_size=kernel_size, padding="same",
                                  activation=tf.nn.relu, kernel_initializer=tf.keras.initializers.he_normal())
        x_2_pool = tf.layers.max_pooling2d(inputs=conv_1, pool_size=2, strides=1)
        keep_prob_1 = tf.cond(is_train, lambda: 0.7, lambda: 1.0)
        dropout = tf.nn.dropout(x_2_pool, keep_prob_1)
        conv_2 = tf.layers.conv2d(inputs=dropout, filters=64, kernel_size=kernel_size, padding="same",
                                  activation=tf.nn.relu, kernel_initializer=tf.keras.initializers.he_normal())
        flatten = tf.layers.flatten(conv_2)
        keep_prob_2 = tf.cond(is_train, lambda: 0.7, lambda: 1.0)
        dropout = tf.nn.dropout(flatten, keep_prob_2)
        return tf.layers.dense(dropout, mapped_num, tf.nn.relu)

    def classifier_layers(self, x, cluster_num, is_train):
        """
        setup classifier layers.
        :param x:
        :param cluster_num:
        :param is_train: tf.bool, if train or other.
        :return: a network that outputs "cluster_num" number neuron.
        """
        dense = tf.layers.dense(x, units=cluster_num, activation=tf.nn.relu,
                                kernel_initializer=tf.keras.initializers.he_normal())
        keep_prob = tf.cond(is_train, lambda: 0.7, lambda: 1.0)
        dropout = tf.nn.dropout(dense, keep_prob)
        return tf.layers.dense(dropout, units=cluster_num, activation=tf.nn.softmax,
                               kernel_initializer=tf.keras.initializers.he_normal())


if __name__ == "__main__":
    from tensorflow.examples.tutorials.mnist import input_data
    import warnings

    warnings.filterwarnings('ignore')

    # operation check using MNISt.
    train_data_size = 3000
    test_data_size = 1000
    mnist = input_data.read_data_sets('MNIST_DA0A/')
    # train data
    x, y = mnist.train.next_batch(train_data_size)
    x = np.reshape(x, (train_data_size, 28, 28, 1))
    # test data
    test_x, test_y = mnist.train.next_batch(test_data_size)
    test_x = np.reshape(test_x, (test_data_size, 28, 28, 1))

    deep_cluster = SimpleDeepCluster(10)
    deep_cluster.init_model(x[0].shape, 200, 1e-5)
    deep_cluster.train(x, 500, y, test_x, test_y, rebuild_kmeans_epochs=1)
    deep_cluster.save("./deepcluster")
