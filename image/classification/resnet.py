import numpy as np
from sklearn.metrics import accuracy_score
import tensorflow as tf
from collections import OrderedDict
from typing import List

from common.base_model import BaseModelUsingExternalStorage
from common.evaluation import Evaluation
from utils.common_utils import BatchIterator


class ResNet(BaseModelUsingExternalStorage):
    LOSS_VALUE_KEY = "loss"
    EVALUATION_VALUE_KEY = "accuracy"
    # 何ブロックごとにチャンネル数を増やすか
    INCREASING_NUM_OF_CHANNELS_PER_BLOCK = 4

    def __init__(self, input_dim: tuple, output_num: int, learning_rate: float, block_num=10):
        """
        :param input_dim:
        :param output_num:
        :param learning_rate:
        :param block_num: Resnetブロックの数
        """
        super().__init__()
        tf.reset_default_graph()
        # 入力
        self._x = tf.placeholder(tf.float32, (None,) + input_dim)
        self._is_training = tf.placeholder(dtype=tf.bool)
        # 2次元ならconv2dに合うよう3次元に変換する
        if len(input_dim) == 2:
            x = tf.reshape(self._x, shape=(-1,) + input_dim + (1,))
        else:
            x = self._x
        self._prediction_result = self._cnn_layer(x, output_num, block_num, self._is_training)
        self._t = tf.placeholder(tf.float32, [None, output_num])
        self._cross_entropy = -tf.reduce_mean(self._t * tf.log(self._prediction_result + 1e-12))
        self._train_step = tf.train.AdamOptimizer(learning_rate).minimize(self._cross_entropy)
        # グラフ構築
        gpu_config = tf.ConfigProto(
            gpu_options=tf.GPUOptions(per_process_gpu_memory_fraction=0.8,
                                      allow_growth=True),
            device_count={'GPU': 1})
        self._session = tf.Session(config=gpu_config)
        init = tf.global_variables_initializer()
        self._session.run(init)

    def _cnn_layer(self, x, output_num, layer_num, is_training):
        """
        CNNレイヤー
        :param x: 
        :param output_num: 
        :param is_training: 
        :return: 
        """
        out = x
        for i in range(layer_num):
            channel_num = i // self.INCREASING_NUM_OF_CHANNELS_PER_BLOCK + 1
            out = self._resnet_block(out, 32 * channel_num, is_training, downsample=True,
                                     scope='resnet_block_{}'.format(i + 1))
        global_pool = self._global_average_pooling(out, output_num)
        return tf.nn.softmax(global_pool)

    def _global_average_pooling(self, x, output_num):
        x = self._conv(x, output_num, kernel=1, stride=1, scope='output')
        for _ in range(2):
            x = tf.reduce_mean(x, axis=1)
        return x

    def _resnet_block(self, x_init, channels, is_training, downsample=False, upsample=False,
                      scope='resnet_block'):
        keep_prob = tf.cond(is_training, lambda: 0.6, lambda: 1.0)
        with tf.variable_scope(scope):
            # 1x1 Convlution
            x = self._batch_norm(x_init, scope='batch_norm_1x1_front')
            shortcut = tf.nn.relu(x)
            x = self._conv(shortcut, channels, kernel=1, stride=1, scope='conv_1x1_front')

            # 3x3 Convolution
            x = self._batch_norm(x, scope='batch_norm_3x3_front')
            x = tf.nn.relu(x)
            if downsample:
                x = self._conv(x, channels, kernel=3, stride=1, scope='conv_3x3')
                shortcut = self._conv(shortcut, channels * 4, kernel=1, stride=1, scope='shortcut_restoration_depth')
            elif upsample:
                x = self._conv_transpose(x, channels, kernel=3, stride=1, scope='conv_3x3')
                shortcut = self._conv_transpose(shortcut, channels * 4, kernel=1, stride=1,
                                                scope='shortcut_restoration_depth')
            else:
                x = self._conv(x, channels, kernel=3, stride=1, scope='conv_3x3')
                shortcut = self._conv(shortcut, channels * 4, kernel=1, stride=1, scope='shortcut_restoration_depth')

            # 1x1 Convolution. restoration depth.
            x = self._batch_norm(x, scope='batch_norm_1x1_back')
            x = tf.nn.relu(x)
            x = tf.nn.dropout(x, keep_prob)
            x = self._conv(x, channels * 4, kernel=1, stride=1, scope='restoration_depth_conv')
            return x + shortcut

    def _conv(self, x, channels, kernel=4, stride=2, scope='conv_0'):
        with tf.variable_scope(scope):
            x = tf.layers.conv2d(
                inputs=x,
                filters=channels,
                kernel_size=kernel,
                strides=stride,
                use_bias=True,
                padding="same")
            return x

    def _conv_transpose(self, x, channels, kernel=4, stride=2, scope='conv_0'):
        with tf.variable_scope(scope):
            x = tf.layers.conv2d_transpose(
                inputs=x,
                filters=channels,
                kernel_size=kernel,
                strides=stride,
                use_bias=True,
                padding="same")
            return x

    def _batch_norm(self, x, scope='batch_norm'):
        return tf.contrib.layers.batch_norm(
            x,
            decay=0.9,
            epsilon=1e-05,
            center=True,
            scale=True,
            updates_collections=None,
            is_training=True,
            fused=False,
            scope=scope)

    def train(self, x_list, y_list, epochs, test_x_list=None, test_y_list=None, batch_size=30):
        print("********** training start **********")
        data_len = x_list.shape[0]
        for epoch in range(epochs):
            for batch_index_start, batch_index_end in BatchIterator(0, data_len, batch_size):
                self._session.run(self._train_step, feed_dict={self._is_training: True,
                                                               self._x: x_list[batch_index_start: batch_index_end],
                                                               self._t: y_list[batch_index_start: batch_index_end]})
            self._output_progress(epoch, x_list, y_list, test_x_list, test_y_list, batch_size)
        print("********** training end **********")

    def evaluate(self, x_list, y_list, batch_size: int = 30) -> Evaluation:
        loss_value_sum = 0
        batch_count = 0
        prediction_result_list = None
        data_len = x_list.shape[0]
        for batch_index_start, batch_index_end in BatchIterator(0, data_len, batch_size):
            loss_value, batch_prediction_result = \
                self._session.run([self._cross_entropy, self._prediction_result],
                                  feed_dict={self._is_training: False,
                                             self._x: x_list[batch_index_start:batch_index_end],
                                             self._t: y_list[batch_index_start:batch_index_end]})
            loss_value_sum += loss_value
            if batch_index_start == 0:
                prediction_result_list = batch_prediction_result
                continue
            prediction_result_list = np.concatenate((prediction_result_list, batch_prediction_result), axis=0)
            batch_count += 1

        evaluate_dict = OrderedDict()
        evaluate_dict[self.LOSS_VALUE_KEY] = loss_value_sum / batch_count
        evaluate_dict[self.EVALUATION_VALUE_KEY] = accuracy_score(np.argmax(y_list, axis=1),
                                                                  np.argmax(prediction_result_list, axis=1))
        return Evaluation(evaluate_dict)

    def predict(self, x, batch_size: int = 30):
        """
        各ラベルの推論結果の確率のリストを返す.
        :param x:
        :param batch_size:
        :return:
        """
        prediction_result_list = None
        data_len = x.shape[0]
        for batch_index_start, batch_index_end in BatchIterator(0, data_len, batch_size):
            batch_prediction_result = self._session.run(self._prediction_result,
                                                        feed_dict={self._is_training: False,
                                                                   self._x: x[batch_index_start: batch_index_end]})
            if batch_index_start == 0:
                prediction_result_list = batch_prediction_result
                continue
            prediction_result_list = np.concatenate((prediction_result_list, batch_prediction_result), axis=0)
        return prediction_result_list

    def train_using_external_storage(self, x_file_path_list, y_list, epochs, test_x_file_path_list=None,
                                     test_y_list=None, batch_size=30):
        print("********** training start **********")
        data_len = len(x_file_path_list)
        for epoch in range(epochs):
            for batch_index_start, batch_index_end in BatchIterator(0, data_len, batch_size):
                batch_data = \
                    super()._get_numpy_data_from_file_path_list(x_file_path_list[batch_index_start: batch_index_end])
                self._session.run(self._train_step, feed_dict={self._is_training: True,
                                                               self._x: batch_data,
                                                               self._t: y_list[batch_index_start: batch_index_end]})
            self._output_progress_using_external_storage(epoch, x_file_path_list,
                                                         y_list, test_x_file_path_list, test_y_list, batch_size)
        print("********** training end **********")

    def predict_using_external_storage(self, x_file_path_list: List[str], batch_size: int = 30):
        """
        各ラベルの推論結果の確率のリストを返す.
        :param x_file_path_list:
        :param batch_size:
        :return:
        """
        prediction_result_list = None
        data_len = len(x_file_path_list)
        for batch_index_start, batch_index_end in BatchIterator(0, data_len, batch_size):
            batch_data = \
                super()._get_numpy_data_from_file_path_list(x_file_path_list[batch_index_start: batch_index_end])
            batch_prediction_result = self._session.run(self._prediction_result,
                                                        feed_dict={self._is_training: False,
                                                                   self._x: batch_data})
            if batch_index_start == 0:
                prediction_result_list = batch_prediction_result
                continue
            prediction_result_list = np.concatenate((prediction_result_list, batch_prediction_result), axis=0)
        return prediction_result_list

    def evaluate_using_external_storage(self, x_file_path_list: List[str], y_list, batch_size: int = 30) -> Evaluation:
        prediction_result_list = None
        data_len = len(x_file_path_list)
        loss_value_sum = 0
        batch_count = 0
        for batch_index_start, batch_index_end in BatchIterator(0, data_len, batch_size):
            batch_data = \
                super()._get_numpy_data_from_file_path_list(x_file_path_list[batch_index_start: batch_index_end])
            loss_value, batch_prediction_result = \
                self._session.run([self._cross_entropy, self._prediction_result],
                                  feed_dict={self._is_training: False,
                                             self._x: batch_data,
                                             self._t: y_list[batch_index_start:batch_index_end]})
            loss_value_sum += loss_value
            if batch_index_start == 0:
                prediction_result_list = batch_prediction_result
                continue
            prediction_result_list = np.concatenate((prediction_result_list, batch_prediction_result), axis=0)
            batch_count += 1

        evaluate_dict = OrderedDict()
        evaluate_dict[self.LOSS_VALUE_KEY] = loss_value_sum / batch_count
        evaluate_dict[self.EVALUATION_VALUE_KEY] = accuracy_score(np.argmax(y_list, axis=1),
                                                                  np.argmax(prediction_result_list, axis=1))
        return Evaluation(evaluate_dict)
