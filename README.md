# DeepStackLib
汎用機械学習ライブラリ

## 内容
- ニューラルネットワークの基本的モデル
- 下処理、data augmentation

## ディレクトリ構成
- DeepStackLib
  - 画像ならimage、時系列ならtime_seriesなど、データ形式によって別れる
  - testsはテストコードのディレクトリ
  - データ形式直下
    - classification: 分類モデル
    - generation: 生成モデル
    - recognition: 認識モデル
    - preprocessor: 下処理
