from pydub import AudioSegment
import numpy as np
from scipy import signal


def trans_spectrogram_list(file_list, adjust_length=None, sampling_num=256):
    """
    音声ファイルリストをスペクトログラムのリストに変換する
    :param file_list: ファイル名のリスト
    :param adjust_length: 指定したらデータの長さを調整する。この数値以上のデータ長なら削り、満たない場合はゼロパディングする。
    :param sampling_num:
    :return: スペクトログラムのリスト
    """
    spectrogram_list = []
    for file_name in file_list:
        spectrogram_list.append(trans_spectrogram(file_name, adjust_length, sampling_num))
    return np.array(spectrogram_list)


def trans_spectrogram(filename, adjust_length=None, sampling_num=256):
    """
    音声ファイルをスペクトログラムに変換する
    :param filename: ファイル名
    :param adjust_length: 指定したらデータの長さを調整する。この数値以上のデータ長なら削り、満たない場合はゼロパディングする。
    :param sampling_num:
    :return: スペクトログラム
    """
    sound = AudioSegment.from_file(filename, filename[filename.rfind(".")+1:])
    sample = np.array(sound.get_array_of_samples())[::sound.channels]
    fs = sound.frame_rate
    f, t, spectrogram = signal.spectrogram(sample, fs, nperseg=sampling_num)
    # 正規化する
    spectrogram /= spectrogram.max()
    return get_adjusted_spectrogram_length(spectrogram.transpose(1, 0), adjust_length)


def get_adjusted_spectrogram_length(data, adjust_length):
    """
    データの長さを調整したスペクトログラムを返す
    :param data: スペクトログラム
    :param adjust_length: 調整するデータ長
    :return: データの長さを調整したスペクトログラム
    """
    if len(data.shape) != 2:
        raise RuntimeError("スペクトログラムは2次元配列である必要があります")
    if adjust_length is None:
        return data
    if data.shape[0] > adjust_length:
        # 指定したデータ長より長ければ削ったデータを返す
        return data[:adjust_length, :]
    # 指定したデータ長に満たなければゼロパディングして返す
    return np.pad(data, [(0, adjust_length - data.shape[0]), (0, 0)], "constant")
