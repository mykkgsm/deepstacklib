import sys
import os
import shutil
import numpy as np


def output_test_name():
    """
    実行するテストの名前を出力する
    スタックトレースの1つ上の関数名を用いることで実現
    """
    func_name = sys._getframe(1).f_code.co_name
    print("\n\n--------------- {} ---------------\n\n".format(str(func_name)))


def delete_dir_recursive(dir_path):
    """
    空ではないディレクトリを全削除
    :param dir_path:
    """
    shutil.rmtree(dir_path)


def delete_tf_model_files(file_name):
    """
    保存したTensorflowモデルの削除
    :param file_name:
    """
    os.remove("./" + file_name + ".data-00000-of-00001")
    os.remove("./" + file_name + ".index")
    os.remove("./" + file_name + ".meta")
    os.remove("./checkpoint")


def get_dummy_array_data_for_classification(train_data_size=100, test_data_size=20, data_dim=(100, 120, 3),
                                            output_num=3):
    """
    分類問題用のダミーデータを取得する
    :param train_data_size: 訓練データのサイズ
    :param test_data_size: テストデータのサイズ
    :param data_dim: データのshape
    :param output_num: 出力データの次元数
    :return: 訓練データ、訓練ラベル、テストデータ、テストラベル
    """
    if len(data_dim) == 1:
        _x = np.random.rand(train_data_size, data_dim[0])
        _test_x = np.random.rand(test_data_size, data_dim[0])
    elif len(data_dim) == 2:
        _x = np.random.rand(train_data_size, data_dim[0], data_dim[1])
        _test_x = np.random.rand(test_data_size, data_dim[0], data_dim[1])
    elif len(data_dim) == 3:
        _x = np.random.rand(train_data_size, data_dim[0], data_dim[1], data_dim[2])
        _test_x = np.random.rand(test_data_size, data_dim[0], data_dim[1], data_dim[2])
    else:
        raise RuntimeError("3次元以内が有効")
    _y = np.random.rand(train_data_size, output_num)
    _test_y = np.random.rand(test_data_size, output_num)
    return _x, _y, _test_x, _test_y
