class BatchIterator(object):
    """
    バッチの開始インデックス、終了インデックスを取得するためのイテレータ
    """
    def __init__(self, start_with, data_len, batch_size):
        if start_with >= data_len:
            raise RuntimeError("開始値はデータサイズより小さい必要があります")
        self._data_len = data_len
        self._batch_size = batch_size
        self._batch_index_start = start_with

    def __iter__(self):
        return self

    def __next__(self):
        """
        :return: バッチインデックスの開始値, バッチインデックスの終了値
        """
        if self._batch_index_start == self._data_len:
            raise StopIteration()
        batch_index_start = self._batch_index_start
        if self._batch_index_start + self._batch_size < self._data_len:
            batch_index_end = self._batch_index_start + self._batch_size
        else:
            batch_index_end = self._data_len
        self._batch_index_start = batch_index_end
        return batch_index_start, batch_index_end
