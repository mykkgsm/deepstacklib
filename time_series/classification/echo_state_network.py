import numpy as np
from sklearn.metrics import accuracy_score
import tensorflow as tf

from common.base_model import BaseModel


class SimpleESN(BaseModel):
    """
    未使用のため最新対応がほぼ入ってない
    TODO Cupy, Batch, predict対応
    """
    # ファイル命名用の定数
    RESERVOIR_W_IN_FILE_EXTENSION = "_w_in.npy"
    RESERVOIR_W_FILE_EXTENSION = "_w.npy"
    RESERVOIR_W_OUT_FILE_EXTENSION = "_w_out.npy"

    def __init__(self, input_dim, output_num, reservoir_num, sparsity_rate, learning_rate):
        """
        :param input_dim: 入力時系列データの1つのデータの次元
        :param output_num: 出力次元数
        :param reservoir_num: reservoir層のスパース率
        :param sparsity_rate: reservoir層のdropout率
        :param learning_rate: 学習率
        """
        super().__init__()
        self._w_in = np.zeros((input_dim, reservoir_num), "float32")
        self._w = np.zeros((reservoir_num, reservoir_num), "float32")
        self._w_out = np.zeros((reservoir_num, reservoir_num), "float32")
        self._reservoir_num = reservoir_num
        self._sparsity_rate = sparsity_rate
        self._session = None
        self._init_reservoir_weights()
        self._init_network(output_num, learning_rate)

    def _init_reservoir_weights(self):
        """
        reserviorレイヤの重みを初期化する
        """
        self._w_in = np.random.rand(*self._w_in.shape) - 0.5
        self._w = np.random.rand(*self._w.shape) - 0.5
        self._w_out = np.random.rand(*self._w_out.shape) - 0.5

    def _init_network(self, output_num, learning_rate):
        """
        出力レイヤのネットワーク構築
        :param output_num:
        :param learning_rate:
        :return:
        """
        tf.reset_default_graph()
        self._is_train = tf.placeholder(dtype=tf.bool)
        self._x = tf.placeholder("float32", (None, self._reservoir_num))
        self._prediction_result = self.output_layers(self._x, output_num, self._is_train)
        self._t = tf.placeholder("float32", (None, output_num))
        self._cross_entropy = - tf.reduce_mean(self._t * tf.log(self._prediction_result))
        self._train_step = tf.train.AdamOptimizer(learning_rate).minimize(self._cross_entropy)
        gpu_config = tf.ConfigProto(
            gpu_options=tf.GPUOptions(per_process_gpu_memory_fraction=0.8,
                                      allow_growth=True),
            device_count={'GPU': 1})
        self._session = tf.Session(config=gpu_config)
        init = tf.global_variables_initializer()
        self._session.run(init)

    def output_layers(self, x, output_num, is_train):
        """
        出力層の全結合ニューラルネットワーク構築
        :param x:
        :param output_num: 出力する次元数
        :param is_train:
        :return:
        """
        dense = tf.layers.dense(x, units=100, activation=tf.nn.relu,
                                kernel_initializer=tf.keras.initializers.he_normal())
        keep_prob = tf.cond(is_train, lambda: 0.7, lambda: 1.0)
        dropout = tf.nn.dropout(dense, keep_prob)
        return tf.layers.dense(dropout, units=output_num, activation=tf.nn.softmax)

    def train(self, x_list, y_list, epochs, test_x_list, test_y_list):
        print("********** training start **********")
        for epoch in range(epochs):
            for i in range(len(x_list)):
                mapped_x = self._forward_reservoir_layer(x_list[i], True)
                self._session.run(self._train_step, feed_dict={self._is_train: True,
                                                               self._x: [mapped_x], self._t: [y_list[i]]})
            self._output_progress(epoch, x_list, y_list, test_x_list, test_y_list)
        print("********** training end **********")

    def _forward_reservoir_layer(self, x, is_train=True):
        """
        reservoir層を通したデータを取得する
        :param x: 時系列データ
        :return: reservoir層を通したデータ
        """
        x_pre_t = None
        x_t = None
        for one_series_x in x:
            x_t = np.dot(np.reshape(one_series_x, (1, len(one_series_x))), self._w_in)
            if x_pre_t is not None:
                x_t += x_pre_t
            x_t = np.dot(x_t, self._w)
            x_t = self._reservoir_activation(x_t, is_train)
            x_pre_t = np.copy(x_t)
        return np.reshape(np.dot(x_t, self._w_out), (-1, ))

    def _reservoir_activation(self, x, is_train=True):
        """
        reservoir層の活性化レイヤ
        :param x:
        :param is_train: 訓練中かどうか
        :return: 活性化レイヤを通したデータ
        """
        h = np.tanh(x)
        if is_train:
            # 訓練なら一定確率dropoutする
            dropout_index_list = np.random.randint(0, x.shape[-1] - 1, int(x.shape[-1] * (1.0 - self._sparsity_rate)))
            h[0][dropout_index_list] = 0.0
            # print(h)
        return h

    def accuracy(self, x_list, y_list, batch_size: int=30):
        """
        推論結果の正確性を取得する
        :param x_list:
        :param y_list:
        :param batch_size: 30
        :return:
        """
        mapped_x_list = []
        for x in x_list:
            mapped_x_list.append(self._forward_reservoir_layer(x, False))
        prediction_result_list = self._session.run(self._prediction_result,
                                                   feed_dict={self._is_train: False,
                                                              self._x: mapped_x_list})
        return accuracy_score(np.argmax(y_list, axis=1), np.argmax(prediction_result_list, axis=1))

    def loss(self, x_list, y_list, batch_size: int=30):
        """
        loss値を取得する
        :param x_list:
        :param y_list:
        :return:
        """
        mapped_x_list = []
        for x in x_list:
            mapped_x_list.append(self._forward_reservoir_layer(x, False))
        return self._session.run(self._cross_entropy, feed_dict={self._is_train: False,
                                                                 self._x: mapped_x_list,
                                                                 self._t: y_list})

    def save(self, filepath):
        super().save(filepath)
        np.save(filepath + self.RESERVOIR_W_IN_FILE_EXTENSION, self._w_in)
        np.save(filepath + self.RESERVOIR_W_FILE_EXTENSION, self._w)
        np.save(filepath + self.RESERVOIR_W_OUT_FILE_EXTENSION, self._w_out)

    def load(self, filepath):
        super().load(filepath)
        self._w_in = np.load(filepath + self.RESERVOIR_W_IN_FILE_EXTENSION)
        self._w = np.load(filepath + self.RESERVOIR_W_FILE_EXTENSION)
        self._w_out = np.load(filepath + self.RESERVOIR_W_OUT_FILE_EXTENSION)

    def predict(self, x, batch_size: int=30):
        # TODO 推論実装
        pass