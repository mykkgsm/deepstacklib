import numpy as np
from sklearn.metrics import accuracy_score
from common.base_model import BaseModel


class TwoStepRNNClassifier(BaseModel):
    """
    2段階に分けて分類をするモデル
    例：2つに分類、それぞれのラベルごとに3分割で分類
    """
    def __init__(self, one_step_model, two_step_model_list, one_step_label_num, two_step_label_num):
        """
        :param one_step_model: 1層目のモデル
        :param two_step_model_list: 2層目のモデル
        :param one_step_label_num: 1層目のラベル数
        :param two_step_label_num: 2層目のラベル数
        """
        super().__init__()
        self._one_step_model = one_step_model
        self._two_step_model_list = two_step_model_list
        self._one_step_label_num = one_step_label_num
        self._two_step_label_num = two_step_label_num

    def train(self, x_list, y_list, epochs, test_x_list=None, test_y_list=None):
        one_step_label_list, two_step_label_list = self._get_one_step_label(y_list), self._get_two_step_label(y_list)
        two_step_test_label_list = None
        one_step_test_label_list = None
        if test_y_list is not None:
            one_step_test_label_list, two_step_test_label_list = self._get_one_step_label(
                y_list), self._get_two_step_label(y_list)
        self._one_step_model.train(x_list, one_step_label_list, epochs, test_x_list, one_step_test_label_list)
        for i in range(self._two_step_label_num):
            self._two_step_model_list[i].train(x_list, one_step_label_list, epochs, test_x_list,
                                               two_step_test_label_list)
        print("\n\n\n****************Total Accuracy****************")
        self._output_progress(epochs - 1, x_list, y_list, test_x_list, test_y_list)

    def predict(self, x):
        prediction_result_list = []
        # 1層目の推論をする
        one_step_prediction_result_list = self._one_step_model.predict(x)
        for i in range(len(one_step_prediction_result_list)):
            one_step_label = np.argmax(one_step_prediction_result_list[i])
            # 1層目の推論結果から2層目のどのモデルを使うかを決定し、2層目のモデルの推論をする
            two_step_prediction_result = self._two_step_model_list[one_step_label].predict([x[i]])
            two_step_label = np.argmax(two_step_prediction_result[0])
            prediction_result_list.append(self._get_origin_label(one_step_label, two_step_label))
        return prediction_result_list

    def accuracy(self, x_list, y_list):
        return accuracy_score(np.argmax(y_list, axis=1), self.predict(x_list))

    def loss(self, x_list, y_list):
        pass

    def _get_one_step_label(self, origin_label):
        """
        実ラベルを一層目に使用するラベルに変換する
        :param origin_label: 実ラベル
        :return: 一層目に使用するラベル
        """
        return origin_label // self._two_step_label_num

    def _get_two_step_label(self, origin_label):
        """
        実ラベルを二層目に使用するラベルに変換する
        :param origin_label: 実ラベル
        :return: 二層目に使用するラべる
        """
        return origin_label % self._two_step_label_num

    def _get_origin_label(self, one_step_label, two_step_label):
        """
        1, 2層目のラベルから実ラベルを取得する
        :param one_step_label: 1層目のラベル
        :param two_step_label: 2層目のラベル
        :return: 実ラベル
        """
        return one_step_label * self._two_step_label_num + two_step_label
