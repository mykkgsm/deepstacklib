from collections import OrderedDict
import numpy as np
from sklearn.metrics import accuracy_score
import tensorflow as tf
from enum import Enum, auto

from common.base_model import BaseModel
from common.evaluation import Evaluation
from utils.common_utils import BatchIterator


class CellType(Enum):
    """
    RNNセルのタイプ。
    """
    RNN = auto()
    LSTM = auto()
    GRU = auto()


class RNNClassifier(BaseModel):
    LOSS_VALUE_KEY = "loss"
    EVALUATION_VALUE_KEY = "accuracy"

    def __init__(self, input_dim, output_num, rnn_units, hidden_units, learning_rate, cell_type=CellType.RNN,
                 cell_num=1):
        """
        :param input_dim: 入力時系列データの1つのデータの次元
        :param output_num: 出力次元数
        :param rnn_units: RNN
        :param hidden_units: 隠れ層
        :param learning_rate: 学習率
        """
        super().__init__()
        tf.reset_default_graph()
        self._session = None
        self._is_train = tf.placeholder(dtype=tf.bool)
        self._x = tf.placeholder("float32", (None,) + input_dim)
        rnn_output = self._rnn_layer(self._x, input_dim[0], rnn_units, cell_type, cell_num, self._is_train)
        self._prediction_result = self._fully_connected_classifer_layer(rnn_output, hidden_units, output_num,
                                                                        self._is_train)
        self._t = tf.placeholder("float32", (None, output_num))
        self._cross_entropy = - tf.reduce_mean(self._t * tf.log(self._prediction_result + 1e-12))
        self._train_step = tf.train.AdamOptimizer(learning_rate).minimize(self._cross_entropy)
        gpu_config = tf.ConfigProto(
            gpu_options=tf.GPUOptions(per_process_gpu_memory_fraction=0.8,
                                      allow_growth=True),
            device_count={'GPU': 1})
        self._session = tf.Session(config=gpu_config)
        init = tf.global_variables_initializer()
        self._session.run(init)

    def _rnn_layer(self, x, data_length, rnn_units, cell_type, cell_num, is_train):
        """
        RNNレイヤー
        :param x:
        :param data_length: 1つのデータの長さ
        :param rnn_units: RNNセルの中間層のニューロン数
        :param cell_type: RNNセルのタイプ。RNN, LSTM, GRUが該当する
        :param cell_num: RNNセルをいくつ繋げるか
        :param is_train: 訓練中かどうか
        :return:
        """
        stacked_cell = []
        for _ in range(cell_num):
            if cell_type == CellType.RNN:
                cell = tf.contrib.rnn.BasicRNNCell(rnn_units)
            elif cell_type == CellType.LSTM:
                cell = tf.contrib.rnn.BasicLSTMCell(rnn_units)
            elif cell_type == CellType.GRU:
                cell = tf.contrib.rnn.GRUCell(rnn_units)
            else:
                raise RuntimeError("無効なRNNセルタイプ")
            keep_prob = tf.cond(is_train, lambda: 0.7, lambda: 1.0)
            cell = tf.nn.rnn_cell.DropoutWrapper(cell=cell, output_keep_prob=keep_prob)
            stacked_cell.append(cell)
        cell = tf.nn.rnn_cell.MultiRNNCell(stacked_cell)
        state = cell.zero_state(tf.shape(x)[0], tf.float32)
        outputs = []
        with tf.variable_scope("RNN"):
            for time_step in range(data_length):
                if time_step > 0:
                    tf.get_variable_scope().reuse_variables()
                cell_output, state = cell(x[:, time_step, :], state)
                outputs.append(cell_output)
        return outputs[-1]

    def _fully_connected_classifer_layer(self, x, hidden_units, output_num, is_train):
        """
        全結合層の分類レイヤ
        :param x:
        :param hidden_units: 隠れ層のニューロン数
        :param output_num: 出力ニューロン数
        :param is_train: 訓練中かどうか
        :return:
        """
        hidden_dense = tf.layers.dense(x, units=hidden_units, activation=tf.nn.relu)
        keep_prob = tf.cond(is_train, lambda: 0.6, lambda: 1.0)
        dropout = tf.nn.dropout(hidden_dense, keep_prob)
        return tf.layers.dense(dropout, units=output_num, activation=tf.nn.softmax)

    def train(self, x_list, y_list, epochs, test_x_list=None, test_y_list=None, batch_size=30):
        print("********** training start **********")
        data_len = x_list.shape[0]
        for epoch in range(epochs):
            for batch_index_start, batch_index_end in BatchIterator(0, data_len, batch_size):
                self._session.run(self._train_step, feed_dict={self._is_train: True,
                                                               self._x: x_list[batch_index_start: batch_index_end],
                                                               self._t: y_list[batch_index_start: batch_index_end]})
            self._output_progress(epoch, x_list, y_list, test_x_list, test_y_list, batch_size)
        print("********** training end **********")

    def evaluate(self, x_list, y_list, batch_size: int = 30):
        loss_value_sum = 0
        batch_count = 0
        prediction_result_list = None
        data_len = x_list.shape[0]
        for batch_index_start, batch_index_end in BatchIterator(0, data_len, batch_size):
            loss_value, batch_prediction_result = \
                self._session.run([self._cross_entropy, self._prediction_result],
                                  feed_dict={self._is_train: False,
                                             self._x: x_list[batch_index_start:batch_index_end],
                                             self._t: y_list[batch_index_start:batch_index_end]})
            loss_value_sum += loss_value
            if batch_index_start == 0:
                prediction_result_list = batch_prediction_result
                continue
            prediction_result_list = np.concatenate((prediction_result_list, batch_prediction_result), axis=0)
            batch_count += 1

        evaluate_dict = OrderedDict()
        evaluate_dict[self.LOSS_VALUE_KEY] = loss_value_sum / batch_count
        evaluate_dict[self.EVALUATION_VALUE_KEY] = accuracy_score(np.argmax(y_list, axis=1),
                                                                  np.argmax(prediction_result_list, axis=1))
        return Evaluation(evaluate_dict)

    def predict(self, x, batch_size: int = 30):
        """
        各ラベルの推論結果の確率のリストを返す.
        :param x:
        :param batch_size:
        :return:
        """
        prediction_result_list = None
        data_len = x.shape[0]
        for batch_index_start, batch_index_end in BatchIterator(0, data_len, batch_size):
            batch_prediction_result = self._session.run(self._prediction_result,
                                                        feed_dict={self._is_train: False,
                                                                   self._x: x[batch_index_start: batch_index_end]})
            if batch_index_start == 0:
                prediction_result_list = batch_prediction_result
                continue
            prediction_result_list = np.concatenate((prediction_result_list, batch_prediction_result), axis=0)
        return prediction_result_list
