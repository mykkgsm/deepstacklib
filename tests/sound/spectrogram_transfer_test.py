import unittest, os
from sound.preprocessor import spectrogram_transfer


class SpectrogramTransferTest(unittest.TestCase):
    dir_path = ""  # TODO 音楽ファイルの入ったフォルダのパスを指定

    def test_spectrogram_list_padding(self):
        """
        スペクトログラムリストのゼロパディングをテストする
        :return:
        """
        self._test_spectrogram_list(1000)

    def test_spectrogram_list_shaving(self):
        """
        スペクトログラムリストのリサイズをテストする
        :return:
        """
        self._test_spectrogram_list(10)

    def _test_spectrogram_list(self, adjust_len):
        """
        指定したデータ長のスペクトログラムリストを取得できるかをテストする
        :param adjust_len:
        :return:
        """
        file_list = os.listdir(self.dir_path)
        for i in range(len(file_list)):
            file_list[i] = "{}/{}".format(self.dir_path, file_list[i])
        spectrogram_list = spectrogram_transfer.trans_spectrogram_list(file_list, adjust_len)
        self.assertEqual(spectrogram_list.shape[0], len(file_list))
        self.assertEqual(spectrogram_list.shape[1], adjust_len)
