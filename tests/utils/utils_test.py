import unittest
from utils import common_utils
from utils.test_utils import output_test_name


class UtilsTest(unittest.TestCase):
    def test_batch_iterator_representative_value(self):
        output_test_name()
        expected_start_list = [0, 10, 20]
        expected_end_list = [10, 20, 25]
        start_with, data_len, batch_size = 0, 25, 10
        result_start_list = []
        result_end_list = []
        for batch_start, batch_end in common_utils.BatchIterator(start_with, data_len, batch_size):
            result_start_list.append(batch_start)
            result_end_list.append(batch_end)
        self.assertEqual(result_start_list, expected_start_list, "代表値のバッチインデックス開始値")
        self.assertEqual(result_end_list, expected_end_list, "代表値のバッチインデックス終了値")

    def test_batch_iterator_upper_boundary_value(self):
        output_test_name()

        expected_start_list = [0, 20]
        expected_end_list = [20, 40]
        start_with, data_len, batch_size = 0, 40, 20
        result_start_list = []
        result_end_list = []
        for batch_start, batch_end in common_utils.BatchIterator(start_with, data_len, batch_size):
            result_start_list.append(batch_start)
            result_end_list.append(batch_end)
        self.assertEqual(result_start_list, expected_start_list, "上の境界値のバッチインデックス開始値1")
        self.assertEqual(result_end_list, expected_end_list, "上の境界値のバッチインデックス終了値1")

        expected_start_list = [0, 20]
        expected_end_list = [20, 39]
        start_with, data_len, batch_size = 0, 39, 20
        result_start_list = []
        result_end_list = []
        for batch_start, batch_end in common_utils.BatchIterator(start_with, data_len, batch_size):
            result_start_list.append(batch_start)
            result_end_list.append(batch_end)
        self.assertEqual(result_start_list, expected_start_list, "上の境界値のバッチインデックス開始値2")
        self.assertEqual(result_end_list, expected_end_list, "上の境界値のバッチインデックス終了値2")

    def test_batch_iterator_lower_boundary_value(self):
        output_test_name()

        expected_start_list = [0, 15, 30]
        expected_end_list = [15, 30, 31]
        start_with, data_len, batch_size = 0, 31, 15
        result_start_list = []
        result_end_list = []
        for batch_start, batch_end in common_utils.BatchIterator(start_with, data_len, batch_size):
            result_start_list.append(batch_start)
            result_end_list.append(batch_end)
        self.assertEqual(result_start_list, expected_start_list, "下の境界値のバッチインデックス開始値1")
        self.assertEqual(result_end_list, expected_end_list, "下の境界値のバッチインデックス終了値1")

        expected_start_list = [0, 15, 30]
        expected_end_list = [15, 30, 32]
        start_with, data_len, batch_size = 0, 32, 15
        result_start_list = []
        result_end_list = []
        for batch_start, batch_end in common_utils.BatchIterator(start_with, data_len, batch_size):
            result_start_list.append(batch_start)
            result_end_list.append(batch_end)
        self.assertEqual(result_start_list, expected_start_list, "下の境界値のバッチインデックス開始値2")
        self.assertEqual(result_end_list, expected_end_list, "下の境界値のバッチインデックス終了値2")

    def test_batch_iterator_error(self):
        output_test_name()

        with self.assertRaises(RuntimeError):
            common_utils.BatchIterator(30, 10, 5)
        with self.assertRaises(RuntimeError):
            common_utils.BatchIterator(10, 10, 5)


if __name__ == "__main__":
    unittest.main()