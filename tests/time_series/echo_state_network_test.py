# TODO テスト作成・ESNを最新版に対応
# import unittest
# import numpy as np
# from time_series.model.EchoStateNetwork import SimpleESN
#
#
# def _get_dummy_data():
#     _x = np.array([
#         [
#             [0.1, 1.0, 0.8],
#             [0.2, 0.2, 0.3],
#             [0.5, 0.2, 1.0]
#         ],
#         [
#             [0.5, 1.0, 0.8],
#             [0.4, 0.9, 0.1],
#             [0.2, 0.2, 1.0],
#             [0.1, 0.9, 0.9],
#             [0.5, 0.2, 1.0]
#         ]
#     ])
#     _y = np.array([[0.0, 1.0], [0.0, 1.0]])
#     _test_x = np.array([
#         [
#             [0.5, 1.0, 0.8],
#             [0.2, 0.2, 1.0],
#             [0.5, 0.2, 1.0]
#         ],
#         [
#             [0.5, 1.0, 0.8],
#             [0.4, 0.9, 0.1],
#             [0.2, 0.2, 1.0],
#             [0.1, 0.9, 0.9],
#             [0.5, 0.2, 1.0]
#         ]
#     ])
#     _test_y = np.array([[0.0, 1.0], [0.0, 1.0]])
#     return _x, _y, _test_x, _test_y
#
#
# class FullyConnectedModelUnitTest(unittest.TestCase):
#     def test_training_and_save_load(self):
#         test_file_path = "./testtest"
#         # モデル、データセット、訓練データの初期化
#         _x, _y, _test_x, _test_y = _get_dummy_data()
#         _model = SimpleESN(len(_x[0][0]), _y.shape[-1], 20, 0.7, 0.0001)
#         # 訓練
#         _model.train(_x, _y, 1000, _test_x, _test_y)
#         # ファイル保存
#         _model.save(test_file_path)
#         # モデル再生性
#         _model = SimpleESN(len(_x[0][0]), _y.shape[-1], 20, 0.7, 0.0001)
#         # ファイルからロード
#         _model.load(test_file_path)
#         # 再度訓練
#         _model.train(_x, _y, 100, _test_x, _test_y)
#
#
# if __name__ == "__main__":
#     unittest.main()
