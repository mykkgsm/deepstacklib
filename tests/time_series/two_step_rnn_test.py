# TODO 実装途中
# import unittest
# from time_series.model.TwoStepRNNClassifier import TwoStepRNNClassifier
# import numpy as np
#
#
# class TimeSeriesRNNClassifierTest(unittest.TestCase):
#     def test_label_transfer_modules(self):
#         origin_label_list = np.array([
#             0, 1, 2, 3, 4, 5, 6, 8, 9, 11
#         ])
#         expected_one_label_list = np.array([
#             0, 0, 0, 1, 1, 1, 2, 2, 3, 3
#         ])
#         expected_two_label_list = np.array([
#             0, 1, 2, 0, 1, 2, 0, 2, 0, 2
#         ])
#
#         classifier_mock = TwoStepRNNClassifier(None, None, 4, 3)
#         one_step_label_list = classifier_mock._get_one_step_label(origin_label_list)
#         two_step_label_list = classifier_mock._get_two_step_label(origin_label_list)
#         restored_origin_label_list = classifier_mock._get_origin_label(one_step_label_list, two_step_label_list)
#         for i in range(len(origin_label_list)):
#             self.assertEqual(one_step_label_list[i], expected_one_label_list[i])
#             self.assertEqual(two_step_label_list[i], expected_two_label_list[i])
#             self.assertEqual(restored_origin_label_list[i], origin_label_list[i])
#
#
# if __name__ == "__main__":
#     # unittest.main()
#     pass
