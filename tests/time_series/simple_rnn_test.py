import unittest
import numpy as np

from utils.test_utils import get_dummy_array_data_for_classification, output_test_name, delete_tf_model_files
from time_series.classification.simple_rnn import RNNClassifier, CellType


class RNNClassifierTest(unittest.TestCase):
    def _test_training_and_save_load_model_specified_rnn_config(self, cell_type, cell_num):
        """
        指定されたセルのタイプ、セルの数のモデルをテストする
        :param cell_type: セルのタイプ
        :param cell_num: セルの数
        """
        test_file_path = "./testtest"
        _x, _y, _test_x, _test_y = get_dummy_array_data_for_classification(data_dim=(30, 10))

        _model = RNNClassifier(_x.shape[1:], _y.shape[-1], 20, 10, 0.0001, cell_type, cell_num)
        _model.train(_x, _y, 8, _test_x, _test_y, 3)
        _model.save(test_file_path)

        _model = RNNClassifier(_x.shape[1:], _y.shape[-1], 20, 10, 0.0001, cell_type, cell_num)
        _model.load(test_file_path)
        _model.train(_x, _y, 3, _test_x, _test_y, 1)
        self.assertTrue(np.array_equal(_model.predict(_x, 1).shape, _y.shape), "推論結果の形式テスト")
        delete_tf_model_files(test_file_path)

    def test_training_some_rnn_config(self):
        """
        RNNの複数セルのタイプの組み合わせをテストする
        """
        output_test_name()
        cell_num_list = [1, 3]
        for cell_num in cell_num_list:
            self._test_training_and_save_load_model_specified_rnn_config(CellType.RNN, cell_num)
            self._test_training_and_save_load_model_specified_rnn_config(CellType.LSTM, cell_num)
            self._test_training_and_save_load_model_specified_rnn_config(CellType.GRU, cell_num)


if __name__ == "__main__":
    unittest.main()
