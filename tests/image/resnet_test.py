import unittest
import os
import numpy as np
from image.classification.resnet import ResNet
from utils.test_utils import output_test_name, delete_dir_recursive, get_dummy_array_data_for_classification, \
    delete_tf_model_files


class ResNetTest(unittest.TestCase):
    def test_training_and_save(self):
        """
        学習とモデル保存・読み込みのテスト
        """
        output_test_name()

        test_file_path = "./testtest"
        _x, _y, _test_x, _test_y = get_dummy_array_data_for_classification()

        block_num = 10
        _model = ResNet(_x.shape[1:], _y.shape[-1], 0.001, block_num)
        _model.train(_x, _y, 4, _test_x, _test_y, 3)
        _model.save(test_file_path)

        _model = ResNet(_x.shape[1:], _y.shape[-1], 0.001, block_num)
        _model.load(test_file_path)
        _model.train(_x, _y, 3, _test_x, _test_y, 1)
        self.assertTrue(np.array_equal(_model.predict(_x, 3).shape, _y.shape), "推論結果の形式テスト")
        delete_tf_model_files(test_file_path)

    def test_training_and_save_using_external_storage(self):
        """
        外部ストレージを使ったテスト
        """
        output_test_name()

        _x, _y, _test_x, _test_y = get_dummy_array_data_for_classification()

        # テスト用データ保存
        test_dir_path = "./test_dir"
        os.mkdir(test_dir_path)
        os.chdir("test_dir")
        file_path_list = []
        for i in range(_x.shape[0]):
            file_name = "test_data_{}".format(i)
            file_path_list.append(test_dir_path + "/" + file_name + ".npy")
            np.save(file_name, _x[i])
        os.chdir("../")
        _model = ResNet(_x.shape[1:], _y.shape[-1], 0.001, 10)
        _model.train_using_external_storage(file_path_list, _y, 3, file_path_list, _y, 2)
        self.assertTrue(np.array_equal(_model.predict_using_external_storage(file_path_list, 3).shape,
                                       _y.shape), "推論結果の形式テスト")
        delete_dir_recursive("./test_dir")

    def test_training_2dimension_data(self):
        """
        学習とモデル保存・読み込みのテスト
        """
        output_test_name()

        _x, _y, _test_x, _test_y = get_dummy_array_data_for_classification(data_dim=(100, 50))

        block_num = 5
        _model = ResNet(_x.shape[1:], _y.shape[-1], 0.001, block_num)
        _model.train(_x, _y, 4, _test_x, _test_y, 3)
        self.assertTrue(np.array_equal(_model.predict(_x, 2).shape, _y.shape), "推論結果の形式テスト")


if __name__ == "__main__":
    unittest.main()
