import unittest
import numpy as np
from common import data_augmentation
from two_dimension.preprocessor import TwoDimDataAugment


class DataAugmentTest(unittest.TestCase):
    def test_data_augmentation(self):
        data = [
            np.array([
                [1, 3, 5, 6],
                [1, 2, 0, 19]
            ]),
            np.array([
                [10, 6, 2, 8],
                [1, 10, 10, 10]
            ])
        ]
        file_name_list = ["hoge.txt", "fuga.txt"]
        target_dir = "./test_data"
        data_augment_list = [TwoDimDataAugment.WidthWiseReverseDataAugment(),
                             TwoDimDataAugment.NoiseDataAugment(),
                             TwoDimDataAugment.RightShiftDataAugment(0.5)]
        DataAugment.data_augmentation(data, file_name_list, target_dir, data_augment_list)


if __name__ == "__main__":
    unittest.main()
