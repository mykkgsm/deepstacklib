from setuptools import setup, find_packages

setup(
    name="DeepStackLib",
    author="",
    author_email="",
    version="0.1",
    packages=find_packages(),
    test_suite='tests', install_requires=['numpy', 'pydub', 'scipy', 'scikit-learn', 'tensorflow', 'matplotlib']
)
