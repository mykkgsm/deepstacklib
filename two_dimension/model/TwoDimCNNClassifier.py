import numpy as np
from sklearn.metrics import accuracy_score
import tensorflow as tf
from collections import OrderedDict

from common.BaseModel import BaseModel
from common.Evaluation import Evaluation
from utils.CommonUtils import BatchIterator


class TwoDimCNNClassifier(BaseModel):
    LOSS_VALUE_KEY = "loss"
    EVALUATION_VALUE_KEY = "accuracy"

    def __init__(self, input_dim, output_num, hidden_units, learning_rate):
        """
        :param input_dim: 入力時系列データの1つのデータの次元
        :param output_num: 出力次元数
        :param hidden_units: 隠れ層のニューロン数
        :param learning_rate: 学習率
        """
        super().__init__()
        tf.reset_default_graph()
        self._session = None
        self._is_train = tf.placeholder(dtype=tf.bool)
        self._x = tf.placeholder("float32", (None,) + input_dim)
        cnn_output = self._cnn_layer(self._x, input_dim)
        self._prediction_result = self._fully_connected_classifer_layer(cnn_output, hidden_units, output_num,
                                                                        self._is_train)
        self._t = tf.placeholder("float32", (None, output_num))
        self._cross_entropy = - tf.reduce_mean(self._t * tf.log(self._prediction_result))
        self._train_step = tf.train.AdamOptimizer(learning_rate).minimize(self._cross_entropy)
        gpu_config = tf.ConfigProto(
            gpu_options=tf.GPUOptions(per_process_gpu_memory_fraction=0.8,
                                      allow_growth=True),
            device_count={'GPU': 1})
        self._session = tf.Session(config=gpu_config)
        init = tf.global_variables_initializer()
        self._session.run(init)

    def _cnn_layer(self, x, input_dim):
        """
        CNNレイヤー
        :param x:
        :param input_dim:
        :return:
        """
        x = tf.reshape(x, shape=(-1,) + input_dim + (1,))
        conv1 = tf.layers.conv2d(x, 32, 3, activation=tf.nn.relu, padding="same")
        conv1 = tf.layers.max_pooling2d(conv1, 2, 2)
        conv2 = tf.layers.conv2d(conv1, 32, 3, activation=tf.nn.relu)
        conv2 = tf.layers.max_pooling2d(conv2, 2, 2)
        conv3 = tf.layers.conv2d(conv2, 32, 3, activation=tf.nn.relu)
        return tf.layers.flatten(conv3)

    def _fully_connected_classifer_layer(self, x, hidden_units, output_num, is_train):
        """
        全結合層の分類レイヤ
        :param x:
        :param hidden_units: 隠れ層のニューロン数
        :param output_num: 出力ニューロン数
        :param is_train: 訓練中かどうか
        :return:
        """
        hidden_dense = tf.layers.dense(x, units=hidden_units, activation=tf.nn.relu)
        keep_prob = tf.cond(is_train, lambda: 0.6, lambda: 1.0)
        dropout = tf.nn.dropout(hidden_dense, keep_prob)
        return tf.layers.dense(dropout, units=output_num, activation=tf.nn.softmax)

    def train(self, x_list, y_list, epochs, test_x_list=None, test_y_list=None, batch_size=30):
        print("********** training start **********")
        data_len = x_list.shape[0]
        for epoch in range(epochs):
            for batch_index_start, batch_index_end in BatchIterator(0, data_len, batch_size):
                self._session.run(self._train_step, feed_dict={self._is_train: True,
                                                               self._x: x_list[batch_index_start: batch_index_end],
                                                               self._t: y_list[batch_index_start: batch_index_end]})
            self._output_progress(epoch, x_list, y_list, test_x_list, test_y_list, batch_size)
        print("********** training end **********")

    def evaluate(self, x_list, y_list, batch_size: int = 30):
        loss_value_sum = 0
        batch_count = 0
        prediction_result_list = None
        data_len = x_list.shape[0]
        for batch_index_start, batch_index_end in BatchIterator(0, data_len, batch_size):
            loss_value, batch_prediction_result = \
                self._session.run([self._cross_entropy, self._prediction_result],
                                  feed_dict={self._is_train: False,
                                             self._x: x_list[batch_index_start:batch_index_end],
                                             self._t: y_list[batch_index_start:batch_index_end]})
            loss_value_sum += loss_value
            if batch_index_start == 0:
                prediction_result_list = batch_prediction_result
                continue
            prediction_result_list = np.concatenate((prediction_result_list, batch_prediction_result), axis=0)
            batch_count += 1

        evaluate_dict = OrderedDict()
        evaluate_dict[self.LOSS_VALUE_KEY] = loss_value_sum / batch_count
        evaluate_dict[self.EVALUATION_VALUE_KEY] = accuracy_score(np.argmax(y_list, axis=1),
                                                                  np.argmax(prediction_result_list, axis=1))
        return Evaluation(evaluate_dict)

    def predict(self, x, batch_size: int = 30):
        """
        各ラベルの推論結果の確率のリストを返す.
        :param x:
        :param batch_size:
        :return:
        """
        prediction_result_list = None
        data_len = x.shape[0]
        for batch_index_start, batch_index_end in BatchIterator(0, data_len, batch_size):
            batch_prediction_result = self._session.run(self._prediction_result,
                                                        feed_dict={self._is_train: False,
                                                                   self._x: x[batch_index_start: batch_index_end]})
            if batch_index_start == 0:
                prediction_result_list = batch_prediction_result
                continue
            prediction_result_list = np.concatenate((prediction_result_list, batch_prediction_result), axis=0)
        return prediction_result_list
