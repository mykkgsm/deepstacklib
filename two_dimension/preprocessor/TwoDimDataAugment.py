from common.DataAugment import BaseDataAugment
from skimage import util
import numpy as np


class NoiseDataAugment(BaseDataAugment):
    def gen_augmented_data(self, data) -> np.ndarray:
        return util.random_noise(data)

    def get_augment_method_name(self) -> str:
        return "noise"


class RightShiftDataAugment(BaseDataAugment):
    def __init__(self, shift_rate):
        self.shift_rate = shift_rate

    def gen_augmented_data(self, data) -> np.ndarray:
        """
        :param data:
        :return:
        """
        shift_num = int(self.shift_rate * data.shape[1])
        if shift_num == 0:
            return data
        rolled_array = np.roll(data, shift_num, axis=1)
        rolled_array[:, :shift_num] = 0.0
        return rolled_array

    def get_augment_method_name(self) -> str:
        return "right_shift{}".format(self.shift_rate)


class LeftShiftDataAugment(BaseDataAugment):
    def __init__(self, shift_rate):
        self.shift_rate = shift_rate

    def gen_augmented_data(self, data) -> np.ndarray:
        """
        :param data:
        :return:
        """
        shift_num = int(self.shift_rate * data.shape[1])
        if shift_num == 0:
            return data
        rolled_array = np.roll(data, -shift_num, axis=1)
        rolled_array[:, -shift_num:] = 0.0
        return rolled_array

    def get_augment_method_name(self) -> str:
        return "left_shift{}".format(self.shift_rate)


class UpShiftDataAugment(BaseDataAugment):
    def __init__(self, shift_rate):
        self.shift_rate = shift_rate

    def gen_augmented_data(self, data) -> np.ndarray:
        """
        :param data:
        :return:
        """
        shift_num = int(self.shift_rate * data.shape[0])
        if shift_num == 0:
            return data
        rolled_array = np.roll(data, -shift_num, axis=0)
        rolled_array[-shift_num:, :] = 0.0
        return rolled_array

    def get_augment_method_name(self) -> str:
        return "up_shift{}".format(self.shift_rate)


class DownShiftDataAugment(BaseDataAugment):
    def __init__(self, shift_rate):
        self.shift_rate = shift_rate

    def gen_augmented_data(self, data) -> np.ndarray:
        """
        :param data:
        shift_rate required: 全体の何%シフトするか
        :return:
        """
        shift_num = int(self.shift_rate * data.shape[0])
        if shift_num == 0:
            return data
        rolled_array = np.roll(data, shift_num, axis=0)
        rolled_array[:shift_num, :] = 0.0
        return rolled_array

    def get_augment_method_name(self) -> str:
        return "down_shift{}".format(self.shift_rate)


class WidthWiseReverseDataAugment(BaseDataAugment):
    def gen_augmented_data(self, data) -> np.ndarray:
        return data[:, ::-1]

    def get_augment_method_name(self) -> str:
        return "width_wise_reverse"


class LengthWiseReverseDataAugment(BaseDataAugment):
    def gen_augmented_data(self, data) -> np.ndarray:
        return data[::-1, :]

    def get_augment_method_name(self) -> str:
        return "length_wise_reverse"
