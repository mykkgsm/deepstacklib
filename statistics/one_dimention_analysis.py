import math


def gini_coefficient(data):
    """
    一次元データのジニ係数を返す
    :param data:
    :return:
    """
    average = 0
    for num in data: average += num
    average /= len(data)
    return _all_difference(data) / (2 * len(data) ** 2 * average)


def mean_difference(data):
    """
    一次元データの平均差を返す
    :param data:
    :return:
    """
    return _all_difference(data) / (len(data) ** 2)


def variance(data):
    """
    一次元データの分散を返す
    :param data:
    :return:
    """
    variance_num = 0
    for i in range(len(data)):
        for j in range(len(data)):
            variance_num += abs(data[i] - data[j]) ** 2
    return variance_num / len(data)


def standard_deviation(data):
    """
    一次元データの標準偏差を返す
    :param data:
    :return:
    """
    return math.sqrt(variance(data))


def coefficient_of_variation(data):
    """
    一次元データの変動係数を返す
    :param data:
    :return:
    """
    return standard_deviation(data) / average(data)


def standard_score(data):
    """
    標準化されたデータを返す
    :param data:
    :return:
    """
    average_num = average(data)
    standard_deviation_num = standard_deviation(data)
    standard_scores = []
    for num in data:
        standard_scores.append((num - average_num) / standard_deviation_num)
    return standard_scores


def deviation_score(data):
    """
    偏差値得点リストを返す
    :param data:
    :return:
    """
    deviation_scores = standard_score(data)
    for i in range(len(deviation_scores)):
        deviation_scores[i] = 10 * deviation_scores[i] + 50
    return deviation_scores


def _all_difference(data):
    """
    全データの組み合わせの差を求める
    :param data:
    :return:
    """
    all_differene_num = 0
    for i in range(len(data)):
        for j in range(len(data)):
            all_differene_num += abs(data[i] - data[j])
    return all_differene_num


def average(data):
    """
    平均値を返す
    :param data:
    :return:
    """
    sum = 0
    for num in data: sum += num
    return sum / len(data)


def mode(data):
    """
    モード(最頻値)を返す
    :param data:
    :return:
    """
    return max(data)


def median(data):
    """
    メディアンを返す
    :param data:
    :return:
    """
    data_length = len(data)
    if data_length % 2 == 0:
        return (data[data_length // 2 - 1] + data[data_length // 2]) / 2
    return data[data_length // 2]


def _get_func_name_and_result(func, **kwargs):
    return func.__name__ + ": " + str(func(**kwargs))


if __name__ == "__main__":
    list_a = [0, 3, 3, 5, 5, 5, 5, 7, 7, 10]
    print("data A\ngini: {},  mean_difference: {}".format(gini_coefficient(list_a), mean_difference(list_a)))

    list_b = [0, 1, 2, 3, 4, 5, 5, 7, 8, 9]
    print("data B\n{}\n{}".format(_get_func_name_and_result(standard_score, data=list_b),
                                  _get_func_name_and_result(deviation_score, data=list_b)))
