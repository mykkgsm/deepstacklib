import math
import matplotlib.pyplot as plt


def beta_distribution(theta, alpha, beta):
    """
    ベータ分布
    :param theta:
    :param alpha:
    :param beta:
    :return:
    """
    b = math.gamma(alpha) * math.gamma(beta) / math.gamma(alpha + beta)
    return theta ** (alpha - 1) * (1 - theta) ** (beta - 1) / b


class BetaBayes:
    def __init__(self):
        # 事前分布
        self.alpha, self.beta = None, None
        # 尤度
        self.n, self.r = None, None
        # 事後分布
        self.p, self.q = None, None

    def prior_distribution(self, theta, alpha, beta):
        """
        ベータ分布の事前分布を返す
        :param theta:
        :param alpha:
        :param beta:
        :return:
        """
        self.alpha, self.beta = alpha, beta
        return beta_distribution(theta, alpha, beta)

    def likelihood(self, theta, n, r):
        """
        二項分布の尤度を返す
        :param theta:
        :param n:
        :param r:
        :return:
        """
        self.n, self.r = n, r
        c = math.factorial(n) / (math.factorial(r) * math.factorial(n - r))
        return c * theta ** r * (1 - theta) ** (n - r)

    def posterior_distribution(self, theta):
        """
        事後分布
        :param theta:
        :return:
        """
        self.p = self.alpha + self.r
        self.q = self.beta + self.n - self.r
        return beta_distribution(theta, self.p, self.q)

    def average(self):
        """
        事後分布の平均
        :return:
        """
        return self.p / (self.p + self.q)

    def dispersion(self):
        """
        事後分布の分散
        :return:
        """
        return self.p * self.q / (self.p + self.q) ** 2 * (self.p + self.q + 1)

    def mode(self):
        """
        事後分布のモード(最頻値)
        :return:
        """
        return (self.p - 1) / (self.p - 1 + self.q - 1)


if __name__ == "__main__":
    theta = 10
    bb = BetaBayes()
    bb.prior_distribution(theta, 2, 2)
    bb.likelihood(theta, 2, 2)
    y = []
    theta_list = [i / 100 for i in range(100 + 1)]
    for theta in theta_list:
        y.append(bb.posterior_distribution(theta))
    print("avg: {},  dispersion: {},  mode: {}".format(bb.average(), bb.dispersion(), bb.mode()))
    plt.plot(theta_list, y)
    plt.show()
