from collections import OrderedDict


class Evaluation:
    """
    評価指標のディクショナリをラッピングしたクラス
    """
    def __init__(self, evaluate_dict: OrderedDict):
        self.evaluate_dict = evaluate_dict

    def get_string_for_output(self) -> str:
        output_string_list = []
        for key in self.evaluate_dict.keys():
            output_string_list.append("{0:20}: {1:20}".format(key, str(self.evaluate_dict[key])))
        return '\n'.join(output_string_list)
