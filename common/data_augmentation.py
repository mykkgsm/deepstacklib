from abc import ABCMeta, abstractmethod
import os
import numpy as np
from typing import List


class BaseDataAugment(metaclass=ABCMeta):
    @abstractmethod
    def gen_augmented_data(self, data) -> np.ndarray:
        """
        拡張したデータを生成して返す
        :param data: 元データ
        :return: 拡張されたデータ
        """
        pass

    @abstractmethod
    def get_augment_method_name(self) -> str:
        """
        Data Augment手法の名前を返す。
        返還後のファイル名に使用される。
        :return:
        """
        pass


def get_augmented_datum_list(datum: np.ndarray, data_augment_list: List[BaseDataAugment]) -> List[np.ndarray]:
    """
    1つのデータをdata augmentationして拡張されたデータのリストを返す
    :param datum: 元データ。numpy配列
    :param data_augment_list:  inherit DataAugment. data augmentation手法のリスト
    :return:
    """
    augmented_datum_list = []
    for data_augment in data_augment_list:
        augmented_datum_list.append(data_augment.gen_augmented_data(datum))
    return augmented_datum_list


def save_augmented_datum_list(datum: np.ndarray, augmented_datum_list: List[np.ndarray], file_name: str,
                              target_dir_path: str,
                              data_augment_list: List[BaseDataAugment]):
    """
    data augmentationされた1つのデータを対象のディレクトリに保存する
    :param datum: 元データ
    :param augmented_datum_list: 1つのデータをdata augmentationしたデータリスト。numpy配列のリスト。
    :param file_name: 元データのファイル名
    :param target_dir_path: 保存先のディレクトリのパス
    :param data_augment_list: data augmentation手法のリスト
    """
    str_list_parsed_filename_with_period = file_name.split(".")
    # 拡張子がない場合
    if len(str_list_parsed_filename_with_period) == 1:
        file_extension = ""
    else:
        file_extension = str_list_parsed_filename_with_period[-1]
    file_name_excluded_extension = str_list_parsed_filename_with_period[0]
    datum_dir_path = "{}/{}".format(target_dir_path, file_name_excluded_extension)
    os.mkdir(datum_dir_path)
    # Augmentation後のデータを保存
    for i in range(len(augmented_datum_list)):
        augmented_file_name = "{}_{}.{}".format(data_augment_list[i].get_augment_method_name(),
                                                file_name_excluded_extension, file_extension)
        np.save("{}/{}".format(datum_dir_path, augmented_file_name),  augmented_datum_list[i])
    # 元データの保存
    np.save("{}/{}".format(datum_dir_path, file_name), datum)


def data_augmentation(data, file_name_list: List[str], target_dir_path: str,
                      data_augment_list: List[BaseDataAugment]):
    """
    データセットをdata augmentationし保存する
    :param data: データリスト
    :param file_name_list: データに紐づいたファイル名
    :param target_dir_path: 保存対象のディレクトリパス
    :param data_augment_list: data augmentation手法のリスト
    """
    if type(data) == list:
        data_len = len(data)
    elif type(data) == np.ndarray:
        data_len = data.shape[0]
    else:
        raise RuntimeError("data はNumpy配列かリストでなければいけない")
    for i in range(data_len):
        augmented_datum_list = get_augmented_datum_list(data[i], data_augment_list)
        save_augmented_datum_list(data[i], augmented_datum_list, file_name_list[i], target_dir_path,
                                  data_augment_list)
