import tensorflow as tf
import numpy as np
from abc import ABCMeta, abstractmethod
from typing import List
from common.evaluation import Evaluation


class BaseModel(metaclass=ABCMeta):
    """
    Tensorflowモデルのベース
    """

    def __init__(self):
        self._session = None

    @abstractmethod
    def train(self, *args):
        """
        ネットワークの学習を行う。
        RAM上に展開されている訓練データを用いる。
        :param args:
        :return:
        """
        pass

    @abstractmethod
    def predict(self, x, batch_size: int=30):
        """
        推論を行う。
        :param x: 推論したいデータ
        :param batch_size:
        :return: 推論結果
        """
        pass

    @abstractmethod
    def evaluate(self, x_list, y_list, batch_size: int=30) -> Evaluation:
        """
        性能指標を取得する
        :param x_list:
        :param y_list:
        :param batch_size:
        :return: 性能指標をつめたデータ
        """
        pass

    def save(self, file_path: str):
        """
        モデルを保存する
        TODO 必要ならOverride
        :param file_path:
        """
        saver = tf.train.Saver()
        saver.save(self._session, file_path)

    def load(self, file_path: str):
        """
        モデルを読み込む
        TODO 必要ならOverride
        :param file_path:
        """
        saver = tf.train.Saver()
        saver.restore(self._session, file_path)

    def _output_progress(self, epoch: int, x_list, y_list, test_x_list, test_y_list, batch_size: int=30):
        """
        訓練・テストデータの正確性、lossを出力する
        :param epoch:エポック数
        :param x_list:訓練データ
        :param y_list:訓練データのラベル
        :param test_x_list:テストデータ。デフォルトでNone
        :param test_y_list:テストデータラベル。デフォルトでNone
        """
        print("============== epoch {0:6} ==============".format(str(epoch + 1)))
        print("--------------- train data ---------------")
        print(self.evaluate(x_list, y_list, batch_size).get_string_for_output())
        if test_x_list is not None and test_y_list is not None:
            print("--------------- test  data ---------------")
            print(self.evaluate(test_x_list, test_y_list, batch_size).get_string_for_output())
        print("==========================================\n\n")


class BaseModelUsingExternalStorage(BaseModel):
    """
    Tensorflowモデルのベース。
    学習、正確性取得の際に訓練データを外部記憶から読み込むようにする。
    """

    @abstractmethod
    def train_using_external_storage(self, *args):
        """
        ネットワークの学習を行う。
        訓練データを外部記憶から読み込む。
        :param args:
        :return:
        """
        pass

    @abstractmethod
    def predict_using_external_storage(self, x_file_path_list: List[str], batch_size: int=30):
        """
        推論を行う。
        :param x_file_path_list: 推論したいデータのファイルパスリスト
        :param batch_size:
        :return: 推論結果
        """
        pass

    @abstractmethod
    def evaluate_using_external_storage(self, x_file_path_list, y_list, batch_size: int=30) -> Evaluation:
        """
        性能指標を取得する
        :param x_file_path_list
        :param y_list:
        :param batch_size:
        :return: 性能指標をつめたデータ
        """
        pass

    def _output_progress_using_external_storage(self, epoch: int, x_file_path_list: List[str], y_list,
                                                test_x_file_path_list: List[str], test_y_list, batch_size: int = 30):
        """
        訓練・テストデータの正確性、lossを出力する
        訓練データを外部記憶から読み込む
        :param epoch:エポック数
        :param x_file_path_list:訓練データのファイルパスリスト
        :param y_list:訓練データのラベル
        :param test_x_file_path_list:テストデータのファイルパスリスト。デフォルトでNone
        :param test_y_list:テストデータラベル。デフォルトでNone
        :param batch_size
        """
        print("============== epoch {0:6} ==============".format(str(epoch + 1)))
        print("--------------- train data ---------------")
        print(self.evaluate_using_external_storage(x_file_path_list, y_list, batch_size).get_string_for_output())
        if test_x_file_path_list is not None and test_y_list is not None:
            print("--------------- test  data ---------------")
            print(self.evaluate_using_external_storage(test_x_file_path_list, test_y_list, batch_size)
                  .get_string_for_output())
        print("==========================================\n\n")

    def _get_numpy_data_from_file_path_list(self, file_path_list: List[str]):
        """
        ファイルパスリストからnumpyのデータを取得する
        :param file_path_list:
        :return:
        """
        numpy_data = []
        for file_path in file_path_list:
            numpy_data.append(np.load(file_path))
        return np.array(numpy_data)
